<!DOCTYPE html>
    <head>
        <meta charset="UTF-8" />
        <title>CARDIATRICS BASIC LIFESTYLE ASSESSMENT QUIZ</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
    </head>
    <body>
        <div class="container">
            <section>               
                <div id="container_demo" >
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form  action="mysuperscript.php" autocomplete="on"> 
                                <h1>CARDIATRICS BASIC LIFESTYLE ASSESSMENT QUIZ</h1>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><label for="name" class="name"> Name: </label></td>
                                            <td><input id="enter" name="name" required="required" type="text" placeholder="Enter name"/></td>  
                                        </tr>
                                        <tr>
                                            <td><label for="age" class="age"> Age: </label></td>
                                            <td><input id="enter" name="age" required="require" type="number" 
                                            placeholder="Enter age"/></td>  
                                        </tr> 
                                        <tr>
                                            <td><label for="email" class="uname"> Email: </label></td>
                                            <td><input id="enter" name="email" required="required" type="email" placeholder="Enter email"/></td>  
                                        </tr> 
                                        <tr>
                                            <td><label for="number" class="phone"> Phone: </label></td>
                                            <td><input id="enter" name="phone" required="required" type="number" placeholder="Enter phone number"/></td>  
                                        </tr> 
                                        <tr>
                                            <td><label for="username" class="uname"> Gender: </label></td>
                                            <td>
                                                <input type="radio" name="gender" value="male"> Male
                                                <input type="radio" name="gender" value="female"> Female
                                            </td>  
                                        </tr> 
                                    </tbody>                                   
                                </table>
                            </form>
                        </div>
                    </div>
                </div>  
            </section>

            <section>
                <div id="full_content">
                   <h2 id="content_title">Consider an average week in your life and circle 1 answer that fits you best.</h2>
                    <div>
                        <h3>Physical Activity</h3>

                        <div class="question">
                            <h4>1) In the last week, how many minutes of moderate-intensity exercise did you do?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>0-30 minutes</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>30-90 minutes</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>90 to 150 minutes</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>>150 minutes</span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <div class="question">
                            <h4>2) How many hours do you sit in a day?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>0-3 hours</span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>3-6 hours</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>6-9 hours</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>>9 hours</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                            </div>
                        </div><br><br><br>
                        
                        <h3>Nutrition</h3>
                        <div class="question">
                            <h4>3) On an average week, how many times do you have home cooked meals?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>0-7 times</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>8-14 times</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>15-21 times </span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>>21 times</span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <div class="question">
                            <h4>4) On an average day, how many cups (250ml) of water do you drink?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>0-2 cups</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>2-4 cups</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>4-6 cups</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>>6 cups</span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <h3>Sleep</h3>
                        <div class="question">
                            <h4>5) On an average night, how many hours do you sleep (including naps in the day if any)?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span><4 hours</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>4-6 hours</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>6-8 hours</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>>8 hours</span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <div class="question">
                            <h4>6) Rate the quality of your sleep.</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>I sleep soundly through the night</span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>I wake up 2-3 times</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>I wake up regularly</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>I have trouble falling asleep</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <h3>Stress</h3>
                        <div class="question">
                            <h4>7) In the past month, how often have you felt nervous or stressed?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>Never </span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>Almost Never</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>Sometimes</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>Fairly Often</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <div class="question">
                            <h4>8) In the past month, how often have you felt that difficulties were piling up so high that you could not overcome them?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>Never </span><br>
                                    <p id="grade_choice">( 4 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>Almost Never</span><br>
                                    <p id="grade_choice">( 3 )</p>
                                </div>
                                <div id="choice">
                                    <span>Sometimes</span><br>
                                    <p id="grade_choice">( 2 )</p>
                                </div>
                                <div id="choice">
                                    <span>Fairly Often</span><br>
                                    <p id="grade_choice">( 1 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <h3>Alcohol</h3>
                        <div class="question">
                            <h4>9) Do you drink alcohol? <small id="q_tip">(if no, => Q12)</small></h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>Yes</span>
                                </div>
                                <div id="choice" ">
                                    <span>No</span><br>
                                    <p id="grade_choice">( +5 )</p>
                                </div>
                            </div>
                        </div><br><br><br>

                        <div class="question">
                            <h4>10) If yes to the question above, how many units of alcohol do you have each week? <small id="q_tip">(Male:  >14;  Female: >7 units)</small></h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>Male</span>
                                </div>
                                <div id="choice" ">
                                    <span>Female</span><br>
                                </div>
                            </div><br><br>
                            <small id="q_tip">A standard drink is approximately: 375ml bottle/can of Mid strength 3.5%;  100ml Red / white wine; Hard Liquor 30 ml nip</small>
                        </div>

                        <div class="question">
                            <h4>11) In one sitting, do you drink more than 4 (female) and 5 (male) units of alcohol?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>Yes</span>
                                    <p id="grade_choice">( -10 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>No</span><br>
                                </div>
                            </div>
                        </div><br><br><br>

                        <h3>Smoking</h3>
                        <div class="question">
                            <h4>12) Do you smoke?</h4>
                            <div id="choosing">
                                <div id="choice">
                                    <span>Yes</span>
                                    <p id="grade_choice">( -10 )</p>
                                </div>
                                <div id="choice" ">
                                    <span>No</span><br>
                                </div>
                            </div>
                        </div><br><br><br>

                        <h3 id="total_score">TOTAL SCORE: </h3><br><br><br>

                        <table>
                            <tbody>                            
                                <tr>
                                    <th colspan="3">
                                        How to interpret your score
                                    </th>
                                </tr>
                                <tr>
                                    <th>Score: Below 12</th>
                                    <th>Score: 13 - 25</th>
                                    <th>Score: Above 25</th>
                                </tr>
                                <tr>
                                    <td>
                                        It’s time for a Risk Assessment
                                        It seems that your lifestyle & habits may not be leading towards good health. You are at high risk. Take action now!
                                    </td>
                                    <td>
                                        Please consider a Risk Assessment. There are aspects of your lifestyle that could be optimized to reduce your risks associated with heart disease.
                                    </td>
                                    <td>
                                        Congratulations! You generally have a well balanced lifestyle without too many unhealthy habits. If you’re above 40, you may still want to have a Risk Assessment.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> 
                </div>
            </section>
        </div>
    </body>
</html>